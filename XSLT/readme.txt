Dans draft : 
- modiftag.xsl est une feuille XSLT permettant de rendre l'export XML de Transkribus conforme à la TEI.  
- solve_overlap.xsl est une feuille XSLT pour mettre en place un stratégie de traitement des tags scindés fautivement de part et d'autres des éléments 'lb'. Ce code intégrera à terme modiftag.xsl.   

- page2tei_timeUs.xsl est une feuille XSLT adapté de page2tei.xsl de [DarioK](https://github.com/dariok/page2tei) pour le projet timeUS.
- string-pack.xsl est une feuille XSLT créée par [Dariok](https://github.com/dariok/page2tei) nécessaire pour le fonctionnement de page2tei.xsl
