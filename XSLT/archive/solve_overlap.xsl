<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.tei-c.org/ns/1.0"  xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output method="xml" encoding="utf-8" indent="yes"/>
    
    
    <xsl:template match="/">
        <ul>
            <xsl:for-each select="//p/lb">
                
                <xsl:variable name="preceding_node" select="./preceding-sibling::node()[1]"/>
                <xsl:variable name="following_node" select="./following-sibling::node()[1]"/>
                <xsl:variable name="preceding" select="./preceding-sibling::*[1]"/>
                <xsl:variable name="following" select="./following-sibling::*[1]"/>
                <xsl:variable name="linebreak" select="."/>
                
                <!-- ajouter un test pour vérifier qu'il n'y a pas du text() avec/après le noeud éléments !! -->
                
                <xsl:if test="$preceding and $following">
                    
                    <xsl:if test="$preceding != $preceding_node">
                    <xsl:if test="name($preceding) != 'lb'">              
                        <xsl:if test="name($preceding) = name($following)">
                            <li>
                                <xsl:value-of select="name($preceding)"/>
                                <xsl:text> ; </xsl:text>
                                <!-- <lb facs="{$linebreak/@facs}" n="{$linebreak/@n}"/>
                                <xsl:text> ; </xsl:text> -->
                                <xsl:value-of select="name($following)"/>
                            </li>
                        </xsl:if>
                    </xsl:if>
                    </xsl:if>
                </xsl:if>
            </xsl:for-each>
        </ul>
    </xsl:template>    
    
</xsl:stylesheet>