Le dossier `config/` contient un fichier `config.properties` qui permet d'installer simplement une liste de tags pour Transkribus pour le projet Time Us.  

Le dossier `Archives_(obsolète)` contient des documents de travail obsolètes. 

`Ajouter_balises_Transkribus.docx` et `Ajouter_balises_Transkribus.pdf` sont un premier guide pour l'installation manuelle des tags, sans passer par `config.properties`.  

`tag_Transkribus.md` est une note explicative sur le fonctionnement des tags dans Transkribus.  
