# Note sur l'annotation dans Transkribus (TimeUs)

## Bilan sur les tags prédéfinis dans Transkribus
Les tags prédéfinis dans Transkribus et utiles pour le projet sont : 
+ abbrev : annoter une abréviation
+ date : annoter une date 
+ organization : annoter une organisation
+ place : annoter un lieu
+ person : annoter une personne
+ work (?) : annoter une référence bibliographique
+ unclear (?) : annoter une incertitude (sur la lecture du texte)
+ S'y ajoute l'outil de commentaire. 

Ces tags sont traités de la manière suivante lors de leur export vers le format TEI : 
+ **abbrev** devient toujours le groupe d'éléments `<choice><expan></expan><abbr>...</abbr></choice>` contenant la sélection annotée. Sa propriété "expansion" est utilisé pour indiquer un contenu dans `<expan>`. Si elle est n'est pas remplie, `<expan>` reste vide.  
+ **date** devient un élément `<date>`. Ses propriétés "year", "month" et "day" sont toujours exportées vers les attributs @year, @month et @day, lorsqu'aucune valeur n'est indiquée par l'utilisateur, leur valeur par défaut est "0". 
+ **organization** devient l'élément `<orgName>`. Il ne possède pas de propriété.
+ **place** devient l'élément `<placeName>`. Sa propriété "country" devient le sous-élément `<country>`. En revanche, sa propriété "placeName" n'est pas exportée. 
+ **person** devient l'élément `<persName>`. La propriété "occupation" n'est pas exportée en TEI. Lorsque l'on ajoute des propriétés supplémentaires, celles-ci ne sont pas prises en charge lors de l'export. Ses autres propriétés sont exportés comme suit : 
  + "dateOfBirth" = `<birth>`
  + "dateOfDeath" = `<death>`
  + "firstname" = `<forename>`
  + "lastname" = `<surname>`
  + "notice" = `<notice>`
+ **work** devient l'élement `<work>` et ses propriété "creator", "year" et "title" sont respectivement exportées dans les attributs @creator, @year et @title.  
+ **unclear** devient l'élément `<unclear>` et sa propriété "alternative" devient l'attribut @alternative.
+ les **commentaires** créés grâce à l'outil de commentaire sont exportés au sein d'un élément `<note>` directement accolé à la sélection sur laquelle il porte. 

Le comportement des tags prédéfinis par Transkribus lors de l'export vers le format TEI n'est pas constant et ne respecte souvent pas les règles de la TEI puisqu'il aboutit à des éléments ou des attributs qui n'existent pas en TEI. Par ailleurs, certaines propriétés sont ignorées lors de l'export. Cela laisse penser qu'il y aurait un nombre maximum d'attributs que Transkribus est capable de traiter/générer (5? ). Lorsque l'on crée un nouveau tag ou une nouvelle propriété, Transkribus ne vérifie pas sa validité par rapport à la TEI.  
Le fait que les propriétés ne sont pas systématiquement exportés vers des attributs mais le plus souvent vers des sous-éléments conduit à une redondance inutile et non souhaitée de certains éléments dans l'export.  
Globalement, les tags prédéfinis par Transkribus ne sont non seulement pas suffisants pour le travail d'annotation à réaliser dans le cadre du projet Time Us, mais ils ne sont pas non plus satisfaisants. Leur formulation en anglais peut induire de la confusion pour les annotateurs et leur exports en TEI ne fonctionne pas correctement. Actuellement, du côté de l'interface utilisateur, Transkribus ne permet pas de supprimer les tags et les propriétés par défaut. Il ne permet pas non plus de contrôler leur comportement lors de l'export en TEI. Il permet uniquement d'en ajouter de nouveau, qui seront automatiquement exportés comme des éléments ou des attributs portant le même nom que celui donné au tag.  
Cela me conduit à envisager qu'il faudrait pouvoir cacher/supprimer les tags prédéfinis par Transkribus afin de proposer notre propre set de tags pour le projet Time Us. Par ailleurs, à défaut de pouvoir prévoir un paramétrage de l'export TEI, la solution la plus simple reste d'envisager un traitement XSLT du premier export "TEI" proposé par Transkribus afin de le rendre conforme à la TEI.  
La section qui suit récapitule le jeu de tags envisageable pour les besoins de l'annotation des documents dans le cadre du projet Time Us et qui respecte les règles de la TEI, et fournit une documentation pour argumenter ces choix et détailler plus précisément la transformation XSLT à envisager.  

---

## Tableau récapitulatif
Les tags prédéfinis sont formulés en anglais. On pourrait donc utiliser des termes en français pour les distinguer des tags prédéfinis. Seuls les tags "date", "place" et "organization", dont l'export est relativement satisfaisant et la formulation sans ambiguité, sont conservés. L'utilisation du français permet en outre de proposer aux annotateurs des tags précis qui pourront limiter les risques d'erreur.  
Tous les tags créés pour le projet Time Us seront précédés du préfixe "TU_".  

| Information | Tag Transkribus | Export Transkribus prévu | TEI-compliant |
| ----------- | --------------- | ------------------ | ------------- |
| Rémunération | remuneration | `<remuneration>` | `<rs type="revenu">` |
| Type de rémunération | typeRemuneration | `<typeRemuneration>` | `<rs type="revenu" subtype="">`\* |
| Produit | produit | `<produit>` | `<term type="produit">` |
| Montant | montant @type | `<montant type="">` | `<measure type="">`\* |
| Durée | duree | `<duree>` | `<rs type="duree">` |
| Tâche | tache | `<tache>` | `<term type="tache">` |
| Lieu | Place (prédéfini) | `<placeName>` | `<placeName>` |
| Adresse | adresse | `<adresse>` | `<address>` |
| Organisation | organization (prédéfini) | `<orgName>` | `<orgName>` |
| Date | date (prédéfini) | `<date year="0" month="0" day="0">` | `<date>` |
| Heure | heure | `<heure>` | `<time>` |
| Référence à un document | document @type | `<document type="">` | `<bibl type="">`\* |
| Métier | occupation @normal | `<occupation normal=""` | `<term type="occupation">` |
| Personne | personne | `<personne>` | `<person>` |
| Statut matrimonial | statutMatrimonial | `<statutMatrimonial>` | `<person role="">`|
| *Genre* | personne @sex | `<personne sex="">` | `<person sex="">` |
| Echappement | incertitude | `<incertitude>` | `<certainty degree="0">` |
| Commentaire |  | `<node>` | `<!-- -->`|

+ \* valeurs de **subtype** pour `<rs type="remuneration">` : solde, avance, total, indemnité_judiciaire, indemnité_syndicale, solidarité, ...
+ \* valeurs de **@type** pour `<measure>` : absolute, relative.
+ \* valeurs de **@type** pour `<bibl>` : tarif, loi, presse, règlement, ...

## Documentation des choix 
### Echappement
Afin d'identifier les points problématiques d'annotation, Eric de la Clergerie a signalé l'importance de prévoir un mécanisme d'échappement pour les annotateur.ices qui leur permettent de les signaler pour les traiter au cas par cas. Afin de rendre cela possible, y compris dans le cadre de la TEI, il est possible d'utiliser l'élement `<certainty>`, associé à une valeur zéro pour l'attribut @degree. Idéalement, l'utilisation de ce tag serait assortie d'un commentaire expliquant la raison de l'hésitation. 

### Revenu 
Les informations sur les revenus sont exprimées de manières extrèmement variées. Il semble donc important de prévoir un mécanisme d'annotation relativement souple, qui serait ensuite assorti de tags permettant de préciser des informations qui composent la rémunération. Une durée, un produit, une tâche ou encore un montant est un élément qui peut entrer dans cette composition. Ces éléments peuvent cependant aussi être exprimés dans d'autres contextes, sans que l'on souhaite pour autant les ignorer. L'utilisation des éléments `<rs>` et `<term>`, associé à l'attribut @type, permet de ne pas être confronté à des règles strictes d'imbrication des éléments en TEI. 
Il est possible de leur associer l'attributs @subtype qui peut contenir des informations de classification supplémentaire, comme par exemple la nature du revenu, en se conformant à une liste prédéfinie de valeurs possibles pour cet attribut.  

### Lieu
Plusieurs types d'informations de localisations sont contenues dans les documents, en particulier des adresses associées à des établissements et des noms de villes. La déclinaison de ces éléments permet de disposer d'une granularité plus fine sans alourdir la tâche des annotateur.ices. Les noms de villes peuvent en effet être annotés automatiquement.  
L'identification des adresses d'une part et des lieux à proprement parler d'autre part peut permettre d'établir plus facilement des liens entre les différentes informations. 

### Personne
La TEI propose trois solutions principales pour annoter une personne : 
+ `<persName>`
+ `<person>`
+ `<name type="person">`

L'élément persName est conçu pour être associé au nom d'une personne. Ses attributs principaux se concentrent sur les éléments qui caractérisent ce nom. Ses sous-éléments sont également adaptés à cela. L'élément name est sûrement trop générique dans le contexte de notre annotation car on cherche à identifier les statuts matrimoniaux, voire le genre des personnes.  
L'élément person me paraît tout à fait adapté à cela car il peut être associé aux attributs @sex et @role. Le premier permet d'identifier le genre/sexe de la personne, le second son rôle dans la société. On peut donc y inclure les informations sur le statut matrimonial.  

Compte tenu des problèmes posés lors de l'export du tag prédéfini "person" et dans la mesure où il n'est pas possible de lui ajouter des attributs, il me semble préférable d'utiliser un nouveau tag.  

### Occupation
On ne peut pas utiliser l'élément TEI `<occupation>` pour annoter les métiers car il ne peut être contenu que dans les éléments `<person>`, `<persongrp>` ou `<persona>`. Or les évocations de métiers dans les documents se font au-delà de ces seuls contextes. Je propose donc d'utiliser l'élément `<rs>` assorti d'un @type fixe dont la valeur est "occupation". Dans Transkribus, ce tag pourra être "occupation". La propriété "normal" permet de préciser une formulation plus complète dans le cas d'une phrase de type "les ouvriers du tissage à bras et ceux du tissage mécanique". On aurait alors différents états :  
1. Tagging dans Transkribus : `les {occupation}ouvriers du tissage à bras{/occupation} et {occupation @normal:"ouvriers du tissages mécanique"}ceux du tissage mécanique{/occupation}.`  
2. Export XML : `les <occupation>ouvriers du tissage à bras</occupation> et <occupation normal="ouvriers du tissages mécanique">ceux du tissage mécanique</occupation>.`  
3. Transformation TEI : `les <rs type="occupation">ouvriers du tissage à bras</rs> et <choice><orig><rs type="occupation">ceux du tissage mécanique</rs></orig><reg><rs type="occupation">ouvriers du tissage mécanique</rs></reg></choice>.`  

### Date, durée et heure
L'utilisation du tag "Date" ne devrait pas poser de problème : il s'agit d'identifier des dates significatives dans les documents. Le tag "durée" pourrait être utilisé pour annoter les informations relative à la durée d'une tâche ou d'un travail, afin d'évaluer les rémunérations par rapport à la notion de temps. Le tag "heure" en revanche serait utile pour identifier les éléments permettant de faire ressortir des informations concernant les différents évènements qui marque la vie des travailleurs. Ces trois tags identifient des informations de nature / pour des usages différent(e)s. 
