# -*-coding:Latin-1 -*

with open('occupations-9M5', 'r') as file:
	r_file = file.read()

ponctuation = [',', '.', 'l\'']
diacritic_e = ['é', 'è', 'ê']
diacritic_i = ['î']
diacritic_c = ['ç']
for sign in ponctuation:    
	r_file = r_file.replace(sign, '').lower()
for lettre in diacritic_e:
	r_file = r_file.replace(lettre, 'e')
for lettre in diacritic_i:
	r_file = r_file.replace(lettre, 'i')
for lettre in diacritic_c:
	r_file = r_file.replace(lettre, 'c')

liste_trie = []
liste_metier = r_file.split("-\n")
ecartes = ['president', 'secretaire', 'commissaire', 'procureur', 'redacteur', 'assesseur', 'brigadier', 'prefet', 'conseiller', 'juge', 'commisaire', 'croix- rousse']
for metier in liste_metier:
	metier = metier.replace('les ', '')
	metier = metier.replace('le ', '')
	metier = metier.replace('d\'', '')
	for ecarte in ecartes:
		if ecarte in metier:
			metier = "null"
	if not(metier == 'null'):			
		liste_trie.append(metier)

liste_clean = []
for metier in liste_trie:
	metier = metier.replace('iers', 'ier')
	metier = metier.replace('ieres', 'iere')
	metier = metier.replace('ants', 'ant')
	metier = metier.replace('eurs', 'eur')
	metier = metier.replace('euses', 'euse')
	metier = metier.replace('maitres', 'maitre')
	metier = metier.replace('meres', 'mere')
	metier = metier.replace('patrons', 'patron')
	metier = metier.replace('chefs', 'chef')
	metier = metier.replace('compagnons', 'compagnon')
	metier = metier.replace('atelie', 'atelier')	
	metier = metier.replace('atelierr', 'atelier')
	metier = metier.replace("chef atelier", "chef d'atelier")
	liste_clean.append(metier)


decompte = {}
for metier in liste_clean:
	if not (metier == ''):
		if metier not in decompte:
			decompte[metier] = liste_clean.count(metier)

with open('decompte_9M5.txt', 'w') as rec:
	for metier in decompte:
		rec.write(metier + ' : ' + str(decompte[metier]) + ' ;  \n')
